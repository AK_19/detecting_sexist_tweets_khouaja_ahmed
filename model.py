import pandas as pd
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import classification_report, accuracy_score

def load_data():
    # Chargez les données à l'aide de pandas
    df = pd.read_csv('C:\\Users\\AdminEtu\\Downloads\\preprocessed_data.csv')
    return df


def preprocess_data(df):
    # Convertir la colonne 'lang' en valeurs numériques
    df['lang'] = df['lang'].map({'es': 0, 'en': 1, 'fr': 2})
    
    # Supprimer les lignes contenant des valeurs manquantes dans la colonne 'hard_label'
    df = df.dropna(subset=['hard_label'])
    
    # Séparez les caractéristiques (features) et la variable cible (target)
    # Utiliser les colonnes 'lang', 'soft_label_YES', et 'soft_label_NO' comme features
    X = df[['lang', 'soft_label_YES', 'soft_label_NO']]
    y = df['hard_label']
    
    # Normaliser les caractéristiques
    from sklearn.preprocessing import StandardScaler
    scaler = StandardScaler()
    X = scaler.fit_transform(X)
    
    return X, y


def train_model(X_train, y_train):
    # Créer un modèle de régression logistique
    model = LogisticRegression(random_state=42)

    # Définir les hyperparamètres à optimiser
    params = {
    'C': [0.001, 0.01, 0.1, 1, 10],
    'penalty': ['l1', 'l2'],
    'solver': ['saga'],
    'max_iter': [1000] # tentative d'augmentation du 'max_iter en vain (passage à 20000 puis à 1000)
}



def main():
    # Charger les données
    df = load_data()

    # Pré-traiter les données
    X, y = preprocess_data(df)

    # Diviser les données en ensembles d'entraînement et de test
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

    # Créer un modèle de régression logistique
    logistic_regression = LogisticRegression()

    # Paramètres pour la recherche par grille
    params = {
        'C': [0.001, 0.01, 0.1, 1, 10],
        'penalty': ['l1', 'l2'],
        'solver': ['saga']
    }

    # Effectuer une recherche par grille avec validation croisée pour trouver les meilleurs paramètres
    grid_search = GridSearchCV(logistic_regression, params, cv=5)
    grid_search.fit(X_train, y_train)

    print("Best parameters found:")
    print(grid_search.best_params_)
    print("Best score found:")
    print(grid_search.best_score_)

    # Evaluer le modèle sur l'ensemble de test
    y_pred = grid_search.predict(X_test)

    print("Classification Report (best model):")
    print(classification_report(y_test, y_pred))

    print("Accuracy Score (best model):")
    print(accuracy_score(y_test, y_pred))

if __name__ == "__main__":
    main()
