import json
import pandas as pd
import re

# Charger les données JSON à partir du fichier
with open("C:\\Users\\AdminEtu\\Downloads\\fichier_original.json", "r", encoding="utf-8") as file:
    original_data = json.load(file)
with open("C:\\Users\\AdminEtu\\AppData\\Local\\Temp\\peazip-tmp\\.ptmp1ACADA\\training_task1_gold_soft.json", "r") as file:
    V_soft_data = json.load(file)
with open("C:\\Users\\AdminEtu\\AppData\\Local\\Temp\\peazip-tmp\\.ptmp1ACADA\\training_task1_gold_hard.json", "r") as file:
    V_hard_data = json.load(file)

# Convertir les données JSON en un DataFrame pandas
tweets_data = []
for tweet_id, tweet_info in original_data.items():
    tweets_data.append([tweet_id, tweet_info["lang"], tweet_info["tweet"], tweet_info["number_annotators"], tweet_info["labels_task1"]])

df = pd.DataFrame(tweets_data, columns=["id_EXIST", "lang", "tweet", "number_annotators", "labels_task1"])

# Fusionner les données V_soft et V_hard avec df
for id_exist, row in V_soft_data.items():
    df.loc[df["id_EXIST"] == id_exist, "soft_label_YES"] = row["soft_label"]["YES"]
    df.loc[df["id_EXIST"] == id_exist, "soft_label_NO"] = row["soft_label"]["NO"]

for id_exist, row in V_hard_data.items():
    df.loc[df["id_EXIST"] == id_exist, "hard_label"] = row["hard_label"]

print(df.head())

# Pré-traiter les tweets
def preprocess_tweet(tweet):
    # Supprimer les mentions d'utilisateur
    tweet = re.sub(r"@[A-Za-z0-9_]+", "", tweet)
    # Supprimer les URL
    tweet = re.sub(r"http\S+|www\S+|https\S+", "", tweet, flags=re.MULTILINE)
    # Supprimer les caractères spéciaux et les chiffres
    tweet = re.sub(r"\W", " ", tweet)
    # Supprimer les espaces multiples
    tweet = re.sub(r"\s+", " ", tweet).strip()

    return tweet

# Appliquer la fonction de pré-traitement à chaque tweet
df["preprocessed_tweet"] = df["tweet"].apply(preprocess_tweet)

print(df.head())

df.to_csv("preprocessed_data.csv", index=False)
